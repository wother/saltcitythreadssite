# Salt City Threads Static Site

Here we have the Salt City Threads static website. Simple enough,
bootstrap enabled, mobile friendly, links to social and interactive
catalog. A contact website that has minimal moving parts and minimal
dependencies for ease of upkeep.

Code is covered under MIT licence, while copy is copyright 
Salt City Threads 2021.

Base template was from

https://templatemo.com/tm-525-the-town

Assets are custom.